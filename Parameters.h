#ifndef PARAMETERS_H
#define PARAMETERS_H

class Parameters {
public:
    static unsigned GetPopulationSize();
    static unsigned GetRandomSeed();
    
    static void SetPopulationSize(unsigned);
    static void SetRandomSeed(unsigned);

private:
    static unsigned mPopulationSize;
    static unsigned mRandomSeed;
    Parameters();
};

#endif /* PARAMETERS_H */

