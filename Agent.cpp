#include "Agent.h"
#include "RandomSFMT.h"
#include "Parameters.h"

#include <math.h>

Agent::Agent(double x, double y, double diameter) {
    mX = x;
    mY = y;
    mDiameter = diameter;

    mInfected = false;

    mRadius = diameter / 2;
    mArea = M_PI * std::pow(mRadius, 2);
    mVolume = (4 / 3) * mArea * mRadius;
    
    double randomAngle = RandomSFMT::Get()->GetUniform() * 360;
    
    mXSpeed = std::cos((randomAngle * M_PI) / 180);
    mYSpeed = std::sin((randomAngle * M_PI) / 180);
}

    double Agent::GetX() {
        return mX;
    }
    
    double Agent::GetY() {
        return mY;
    }
    
    double Agent::GetDiameter() {
        return mDiameter;
    }
    
    bool Agent::IsInfected() {
        return mInfected;
    }
    
    double Agent::GetRadius() {
        return mRadius;
    }
    
    double Agent::GetArea() {
        return mArea;
    }
    
    double Agent::GetVolume() {
        return mVolume;
    }
    
    double Agent::GetXSpeed() {
        return mXSpeed;
    }
    
    double Agent::GetYSpeed() {
        return mYSpeed;
    }
    
    
    void Agent::SetX(double x) {
        mX = x;
    }
    
    void Agent::SetY(double y) {
        mY = y;
    }
    
    void Agent::SetDiameter(double diameter) {
        mDiameter = diameter;
    }
    
    void Agent::SetInfected(bool infected) {
        mInfected = infected;
    }
    
    void Agent::SetXSpeed(double xSpeed) {
        mXSpeed = xSpeed;
    }
    
    void Agent::SetYSpeed(double ySpeed) {
        mYSpeed = ySpeed;
    }
    