#include "Parameters.h"

unsigned Parameters::mPopulationSize = 300;
unsigned Parameters::mRandomSeed = 0;

Parameters::Parameters() {
}

unsigned Parameters::GetPopulationSize() {
    return mPopulationSize;
}

unsigned Parameters::GetRandomSeed() {
    return mRandomSeed;
}

void Parameters::SetPopulationSize(unsigned populationSize) {
    mPopulationSize = populationSize;
}

void Parameters::SetRandomSeed(unsigned randomSeed) {
    mRandomSeed = randomSeed;
}