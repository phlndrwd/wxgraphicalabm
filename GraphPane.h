/* 
 * File:   GraphPane.h
 * Author: philju
 *
 * Created on 02 April 2020, 15:43
 */

#ifndef GRAPHPANE_H
#define GRAPHPANE_H

#include "Population.h"

#include "wx/wx.h"
#include "wx/sizer.h"

class GraphPane : public wxPanel {
public:
    GraphPane(wxFrame*, Population*);
    
    void Redraw();

    void OnPaint(wxPaintEvent&);
    void OnEraseBackground(wxPaintEvent&);
    void OnMouse(wxMouseEvent&);

    DECLARE_EVENT_TABLE()

private:
    void Draw(wxDC&);
    void PaintBackground(wxDC&);
    
    Population* mPopulation;
    bool mThreadingIsRunning;
};

#endif
