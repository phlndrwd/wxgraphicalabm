/*
 * File:   Point.h
 * Author: philju
 *
 * Created on 30 March 2020, 20:52
 */

#ifndef POINT_H
#define POINT_H

#include <wx/wx.h>

class MainFrame : public wxFrame {
public:
    MainFrame(const wxString&);
    ~MainFrame();

    void OnPaint(wxPaintEvent & event);
};

#endif /* POINT_H */

