#include "MainApp.h"

#include "Parameters.h"
#include "Constants.h"

#include <iostream>

IMPLEMENT_APP(MainApp)

bool MainApp::OnInit() {

    mPopulation = new Population();
    mMainFrame = new MainFrame(wxT("LABM"));
    mGraphPane = new GraphPane((wxFrame*) mMainFrame, mPopulation);
    mGraphPane->SetDoubleBuffered(true);
    //wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
    //sizer->Add(mGraphPane, 1, wxEXPAND);
    //mMainFrame->SetSizer(sizer);
    //mMainFrame->SetAutoLayout(true);

    mMainFrame->Show(true);
    SetTopWindow(mMainFrame);

    //mPopulation->Run((wxWindow*)mGraphPane);

    //mPopulation->Run(mMainFrame);

    //delete population;
    //delete draw;

    return true;
}
