#include "MainThread.h"

#include <iostream>

MainThread::MainThread(Population* population, GraphPane* graphPane, bool* threadIsRunning) {
    mPopulation = population;
    mGraphPane = graphPane;
    mThreadIsRunning = threadIsRunning;
}

void* MainThread::Entry() {
    for (unsigned t = 0; t < 5000; ++t) {
        mPopulation->Update();
        mGraphPane->Redraw();
        std::cout << "t=" << t << std::endl;
    }
    *mThreadIsRunning = false;
}