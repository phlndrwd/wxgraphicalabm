#include "Population.h"

#include "RandomSFMT.h"
#include "Parameters.h"
#include "Constants.h"

Population::Population() {
    Populate();
}

Population::~Population() {
    for (unsigned i = 0; i < Parameters::GetPopulationSize(); ++i) {
        delete mAgents[i];
    }
}

void Population::Update() {
    Movement();
    Collision();
}

void Population::Populate() {
    mAgents.clear();
    for (int i = 0; i < Parameters::GetPopulationSize(); ++i) {
        double randomX = RandomSFMT::Get()->GetUniform() * Constants::cFrameWidth;
        double randomY = RandomSFMT::Get()->GetUniform() * Constants::cFrameWidth;
        //double randomDiameter = Parameters.getMinimumDiameter() + (Parameters.getRandom().nextDouble() * (Parameters.getMaximumDiameter() - Parameters.getMinimumDiameter()));
        double randomDiameter = 10;

        mAgents.push_back(new Agent(randomX, randomY, randomDiameter));
    }
    unsigned randIndex = RandomSFMT::Get()->GetUniformInt(mAgents.size() - 1);
    Agent* randomAgent = mAgents[randIndex];
    randomAgent->SetInfected(true);
}

void Population::Movement() {
    for (unsigned i = 0; i < mAgents.size(); ++i) {
        Agent* agent = mAgents[i];

        double newX = agent->GetX() + agent->GetXSpeed();
        double newY = agent->GetY() + agent->GetYSpeed();

        // Apply toroidal geometry
        if (newX < 0) {
            newX = Constants::cFrameWidth + newX;
        } else if (newX > Constants::cFrameWidth) {
            newX = newX - Constants::cFrameWidth;
        }

        if (newY < 0) {
            newY = Constants::cFrameHeight + newY;
        } else if (newY > Constants::cFrameHeight) {
            newY = newY - Constants::cFrameHeight;
        }

        agent->SetX(newX);
        agent->SetY(newY);
    }
}

void Population::Collision() {
    unsigned numberOfAgents = mAgents.size();
    for (unsigned i = 0; i < numberOfAgents; ++i) {
        Agent* firstAgent = mAgents[i];

        for (unsigned j = 0; j < numberOfAgents; ++j) {
            Agent* secondAgent = mAgents[j];

            // TODO: Does not take account for agents on the edge of the grid.
            if (firstAgent != secondAgent) {
                double firstX = firstAgent->GetX();
                double firstY = firstAgent->GetY();
                double secondX = secondAgent->GetX();
                double secondY = secondAgent->GetY();

                double lengthX = firstX - secondX;
                double lengthY = firstY - secondY;
                double distance = std::sqrt((lengthX * lengthX) + (lengthY * lengthY));

                distance = distance - firstAgent->GetRadius() - secondAgent->GetRadius();
                if (distance <= 0) {
                    double firstNewXSpeed = (firstAgent->GetXSpeed() * (firstAgent->GetVolume() - secondAgent->GetVolume()) + (2 * secondAgent->GetVolume() * secondAgent->GetXSpeed())) / (firstAgent->GetVolume() + secondAgent->GetVolume());
                    double firstNewYSpeed = (firstAgent->GetYSpeed() * (firstAgent->GetVolume() - secondAgent->GetVolume()) + (2 * secondAgent->GetVolume() * secondAgent->GetYSpeed())) / (firstAgent->GetVolume() + secondAgent->GetVolume());
                    double secondNewXSpeed = (secondAgent->GetXSpeed() * (secondAgent->GetVolume() - firstAgent->GetVolume()) + (2 * firstAgent->GetVolume() * firstAgent->GetXSpeed())) / (firstAgent->GetVolume() + secondAgent->GetVolume());
                    double secondNewYSpeed = (secondAgent->GetYSpeed() * (secondAgent->GetVolume() - firstAgent->GetVolume()) + (2 * firstAgent->GetVolume() * firstAgent->GetYSpeed())) / (firstAgent->GetVolume() + secondAgent->GetVolume());

                    firstAgent->SetXSpeed(firstNewXSpeed);
                    firstAgent->SetYSpeed(firstNewYSpeed);
                    secondAgent->SetXSpeed(secondNewXSpeed);
                    secondAgent->SetYSpeed(secondNewYSpeed);

                    firstAgent->SetX(firstAgent->GetX() + firstNewXSpeed);
                    firstAgent->SetY(firstAgent->GetY() + firstNewYSpeed);
                    secondAgent->SetX(secondAgent->GetX() + secondNewXSpeed);
                    secondAgent->SetY(secondAgent->GetY() + secondNewYSpeed);

                    if (firstAgent->IsInfected() == true) secondAgent->SetInfected(true);
                    else if (secondAgent->IsInfected() == true) firstAgent->SetInfected(true);
                }
            }
        }
    }
}

Agent* Population::GetAgent(unsigned i) {
    return mAgents[i];
}

unsigned Population::GetNumberOfAgents() {
    return mAgents.size();
}

std::vector<Agent*> Population::GetAgents() {
    return mAgents;
}
