#include "MainFrame.h"
#include "Constants.h"
#include "Parameters.h"
#include "RandomSFMT.h"

#include <stdlib.h>
#include <time.h>

MainFrame::MainFrame(const wxString& title)
: wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(Constants::cFrameWidth, Constants::cFrameHeight)) {
    
    this->Centre();
}

MainFrame::~MainFrame() {

}

