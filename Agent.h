#ifndef AGENT_H
#define AGENT_H

class Agent {
public:
    Agent(double, double, double);
        
    double GetX();
    double GetY();
    double GetDiameter();
    bool IsInfected();
    double GetRadius();
    double GetArea();
    double GetVolume();
    double GetXSpeed();
    double GetYSpeed();
    
    void SetX(double);
    void SetY(double);
    void SetDiameter(double);
    void SetInfected(bool);
    void SetXSpeed(double);
    void SetYSpeed(double);
    
private:
    double mX;
    double mY;
    double mDiameter;

    double mRadius;
    double mArea;
    double mVolume;
    double mXSpeed;
    double mYSpeed;

    bool mInfected;
};

#endif /* AGENT_H */

