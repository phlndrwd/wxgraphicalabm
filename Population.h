/*
 * File:   Population.h
 * Author: philju
 *
 * Created on 01 April 2020, 17:57
 */

#ifndef POPULATION_H
#define POPULATION_H

#include "Agent.h"

#include "wx/wx.h"

#include <vector>

class Population {
public:
    Population();
    ~Population();
    
    void Update();
    
    Agent* GetAgent(unsigned);
    unsigned GetNumberOfAgents();
    std::vector<Agent*> GetAgents();

private:
    void Populate();
    void Movement();
    void Collision();

    std::vector<Agent*> mAgents;
};

#endif /* POPULATION_H */

