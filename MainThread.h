/* 
 * File:   MainThread.h
 * Author: philju
 *
 * Created on 09 April 2020, 11:53
 */

#include "Population.h"
#include "GraphPane.h"

#ifndef MAINTHREAD_H
#define MAINTHREAD_H

class MainThread : public wxThread {
public:
    MainThread(Population*, GraphPane*, bool*);
    virtual void *Entry();
private:
    Population* mPopulation;
    GraphPane* mGraphPane;
    bool* mThreadIsRunning;

};

#endif /* MAINTHREAD_H */

